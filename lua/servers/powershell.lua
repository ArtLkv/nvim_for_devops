local M = {}

M.settings = {
  cmd = { "pwsh", "-NoLogo", "-NoProfile", "-Command", "~/.local/share/nvim/mason/packages/powershell-editor-services/PowerShellEditorServices/Start-EditorServices.ps1 ..." },
  filetypes = { "ps1" },
  single_file_support = true,
}

return M
