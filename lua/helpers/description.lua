local M = {}

function M.make_description(map, desc)
  return vim.tbl_extend("force", map, { desc = desc })
end

return M
