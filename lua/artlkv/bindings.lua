local map = vim.keymap.set
local describe = require("helpers.description").make_description

-- ToggleTerm map: ../artlkv/plugins.lua
-- LSP map's: ../artlkv/lsp.lua
-- Gitsigns map's ../plugins/gitsigns.lua

map(
  "i",
  "jk",
  "<esc>",
  describe({}, "Быстрый способ выйти из INSERT мода")
)

map("n", "<leader>q", "<cmd>qa!<cr>", describe({}, "[Q]uit without saving"))

map("n", "<leader>l", "<cmd>Lazy<cr>", describe({}, "Open the [L]azy"))
map("n", "<leader>m", "<cmd>Mason<cr>", describe({}, "Open the [M]ason"))

map(
  "n",
  "<C-h>",
  "<C-w><C-h>",
  describe(
    {},
    "Улучшенная навигация между окнами: Left"
  )
)
map(
  "n",
  "<C-j>",
  "<C-w><C-j>",
  describe(
    {},
    "Улучшенная навигация между окнами: Down"
  )
)
map(
  "n",
  "<C-k>",
  "<C-w><C-k>",
  describe(
    {},
    "Улучшенная навигация между окнами: Up"
  )
)
map(
  "n",
  "<C-l>",
  "<C-w><C-l>",
  describe(
    {},
    "Улучшенная навигация между окнами: Right"
  )
)

map(
  "n",
  "<S-Left>",
  "<C-w><S-h>",
  describe({}, "Передвинуть окно: Left")
)
map(
  "n",
  "<S-Down>",
  "<C-w><S-j>",
  describe({}, "Передвинуть окно: Down")
)
map(
  "n",
  "<S-Up>",
  "<C-w><S-k>",
  describe({}, "Передвинуть окно: Up")
)
map(
  "n",
  "<S-Right>",
  "<C-w><S-l>",
  describe({}, "Передвинуть окно: Right")
)

map(
  "n",
  "<C-Up>",
  ":resize +2<CR>",
  describe({}, "Растяжение окна: Left")
)
map(
  "n",
  "<C-Down>",
  ":resize -2<CR>",
  describe({}, "Растяжение окна: Down")
)
map(
  "n",
  "<C-Left>",
  ":vertical resize +10<CR>",
  describe({}, "Растяжение окна: Up")
)
map(
  "n",
  "<C-Right>",
  ":vertical resize -10<CR>",
  describe({}, "Растяжение окна: Right")
)

map("v", "<", "<gv", describe({}, "Увеличение отступа"))
map("v", ">", ">gv", describe({}, "Уменьшение отступа"))

map(
  "n",
  "<cr>",
  "<cmd>noh<cr><cr>",
  describe({}, "Убираем подсветку символов")
)

map(
  "n",
  "tk",
  "<cmd>bnext<cr>",
  describe({}, "Переход к следующему табу")
)
map(
  "n",
  "tj",
  "<cmd>bprev<cr>",
  describe({}, "Переход к предыдущему табу")
)
map(
  "n",
  "th",
  "<cmd>bfirst<cr>",
  describe({}, "Переход к первому табу")
)
map(
  "n",
  "tl",
  "<cmd>blast<cr>",
  describe({}, "Переход к последнему табу")
)
map("n", "td", "<cmd>bdelete<cr>", describe({}, "Закрытие таба"))

map(
  "n",
  "TT",
  "<cmd>TransparentToggle<cr>",
  describe({}, "[T]ransparent [T]oggle")
)

map(
  "n",
  "<leader>fo",
  require("telescope.builtin").oldfiles,
  describe({}, "[F]ind [O]ld files")
)
map(
  "n",
  "<leader>ff",
  require("telescope.builtin").find_files,
  describe({}, "[F]ind [F]iles")
)
map(
  "n",
  "<leader>fw",
  require("telescope.builtin").grep_string,
  describe({}, "[F]ind current [W]ord")
)
map(
  "n",
  "<leader>fg",
  require("telescope.builtin").live_grep,
  describe({}, "[F]ind with [G]rep")
)
map(
  "n",
  "<leader>fh",
  require("telescope.builtin").help_tags,
  describe({}, "[F]ind  [H]elp tags")
)
map(
  "n",
  "<leader>fd",
  require("telescope.builtin").diagnostics,
  describe({}, "[F]ind [D]iagnostics")
)
map(
  "n",
  "<leader>fb",
  require("telescope.builtin").buffers,
  describe({}, "[F]ind existing [B]uffers")
)
map(
  "n",
  "<leader>ft",
  "<cmd>TodoTelescope<cr>",
  describe({}, "[F]ind the [T]odo comments")
)
map(
  "n",
  "<leader>fk",
  require("telescope.builtin").keymaps,
  describe({}, "[F]ind the [K]eymaps")
)
map("n", "<leader>fn", function()
  require("telescope").extensions.notify.notify()
end, describe({}, "[F]ind the [N]otification"))
map("n", "<leader>ee", function()
  require("telescope").extensions.file_browser.file_browser()
end, describe({}, "[E]xplorer [E]xtension"))
map("n", "<leader>fr", function()
  require("telescope").extensions.git_worktree.git_worktrees()
end, describe({}, "[F]ind the [R]epo worktree"))
map("n", "<leader>fR", function()
  require("telescope").extensions.git_worktree.create_git_worktree()
end, describe({}, "[F]ind the [R]epo worktree"))


map(
  "x",
  "<leader>re",
  "<cmd>Refactor extract<cr>",
  describe({}, "[R]efactor [E]xtract")
)
map(
  "x",
  "<leader>rf",
  "<cmd>Refactor extract_to_file<cr>",
  describe({}, "[R]efactor extract to [F]ile")
)
map(
  "x",
  "<leader>rv",
  "<cmd>Refactor extract_var<cr>",
  describe({}, "[R]efactor extract [V]ar")
)
map(
  { "x", "n" },
  "<leader>ri",
  "<cmd>Refactor inline_var<cr>",
  describe({}, "[R]efactor [I]nline var")
)
map(
  "n",
  "<leader>rI",
  "<cmd>Refactor inline_func<cr>",
  describe({}, "[R]efactor [I]nline func")
)
map(
  "n",
  "<leader>rb",
  "<cmd>Refactor extract_block<cr>",
  describe({}, "[R]efactor extract [B]lock")
)
map(
  "n",
  "<leader>rf",
  "<cmd>Refactor extract_block_to_file<cr>",
  describe({}, "[R]efactor extract block to [F]ile")
)

map(
  "n",
  "<leader>tt",
  "<cmd>TroubleToggle<cr>",
  describe({}, "[T]rouble [T]oggle")
)
map(
  "n",
  "<leader>tw",
  "<cmd>TroubleToggle workspace_diagnostics<cr>",
  describe({}, "Diagnostics: [T]rouble [W]orkspace")
)
map(
  "n",
  "<leader>td",
  "<cmd>TroubleToggle document_diagnostics<cr>",
  describe({}, "Diagnostics: [T]rouble [D]ocument")
)
map(
  "n",
  "<leader>tl",
  "<cmd>TroubleToggle loclist<cr>",
  describe({}, "Diagnostics: [T]rouble [L]ocal list")
)
map(
  "n",
  "<leader>tq",
  "<cmd>TroubleToggle quickfix<cr>",
  describe({}, "Diagnostics: [T]rouble [Q]uickfix")
)
map(
  "n",
  "gR",
  "<cmd>TroubleToggle lsp_references<cr>",
  describe({}, "Diagnostics: [G]o to LSP [R]eferences")
)

map("n", "<leader>dt", function()
  require("dapui").toggle()
end, describe({}, "[D]ap [T]oggle"))
map(
  "n",
  "<leader>db",
  "<cmd>DapToggleBreakpoint<cr>",
  describe({}, "[D]ap [B]reakpoint")
)
map("n", "<leader>dc", "<cmd>DapContinue<cr>", describe({}, "[D]ap [C]ontinue"))
map("n", "<leader>dr", function()
  require("dapui").open { reset = true }
end, describe({}, "[D]ap [R]eset"))

map(
  "n",
  "<leader>mt",
  "<cmd>MarkdownPreviewToggle<cr>",
  describe({}, "[M]arkdown Preview [T]oggle")
)

map("n", "<leader>r", "<Plug>RestNvim", describe({}, "[R]est"))
