return {
  -- Dependencies
  { "MunifTanjim/nui.nvim" },
  { "nvim-tree/nvim-web-devicons" },
  { "nvim-lua/plenary.nvim" },
  { "nvim-lua/popup.nvim" },
  { "onsails/lspkind-nvim" },
  { "ray-x/guihua.lua" },

  -- Themes
  {
    "ellisonleao/gruvbox.nvim",
    lazy = false,
    priority = 1000,
    config = function()
      require("gruvbox").setup({
        terminal_colors = true,
        bold = true,
        transparent_mode = true,
      })
      vim.cmd [[colorscheme gruvbox]]
    end,
  },

  -- TransparentToggle
  {
    "xiyaowong/nvim-transparent",
    lazy = false,
  },

  -- Alpha starter
  {
    "goolord/alpha-nvim",
    event = "VimEnter",
    config = function()
      require "plugins.starter"
    end,
  },

  -- Notify
  {
    "rcarriga/nvim-notify",
    event = "VimEnter",
    config = function()
      require("notify").setup {
        background_colour = "#000000",
      }
    end,
  },

  -- Noice
  {
    "folke/noice.nvim",
    lazy = false,
    config = function()
      require("noice").setup {}
    end,
  },

  -- Telescope
  {
    "nvim-telescope/telescope.nvim",
    event = "VimEnter",
    dependencies = {
      { "nvim-telescope/telescope-ui-select.nvim" },
      {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
      },
      { "aaronhallaert/advanced-git-search.nvim" },
      { "cljoly/telescope-repo.nvim" },
      { "nvim-telescope/telescope-symbols.nvim" },
      { "nvim-telescope/telescope-file-browser.nvim" },
    },
    config = function()
      require "plugins.tele"
    end,
  },

  -- Treesitter
  {
    "nvim-treesitter/nvim-treesitter",
    dependencies = {
      { "nvim-treesitter/nvim-treesitter-textobjects" },
      {
        "nvim-treesitter/playground",
        lazy = true,
        cmd = "TSPlaygroundToggle",
      },
    },
    event = "BufReadPre",
    config = function()
      require "plugins.treesitter"
    end,
  },

  -- Lualine
  {
    "nvim-lualine/lualine.nvim",
    event = { "BufRead", "BufNewFile" },
    config = function()
      require "plugins.lualine"
    end,
  },

  -- Some Utils
  {
    "windwp/nvim-autopairs",
    event = { "BufRead", "BufNewFile" },
    config = function()
      require("nvim-autopairs").setup {}
    end,
  },
  {
    "tpope/vim-sleuth",
    event = { "BufRead", "BufNewFile" },
  },
  {
    "tpope/vim-surround",
    event = { "BufRead", "BufNewFile" },
  },

  -- Comment
  {
    "numToStr/Comment.nvim",
    event = { "BufRead", "BufNewFile" },
    config = function()
      require("Comment").setup {}
    end,
  },

  -- Todo comments
  {
    "folke/todo-comments.nvim",
    event = "VimEnter",
    config = function()
      require("todo-comments").setup {}
    end,
  },

  -- LSP
  {
    "neovim/nvim-lspconfig",
    lazy = false,
    dependencies = {
      { "williamboman/mason.nvim" },
      { "williamboman/mason-lspconfig.nvim" },
      { "j-hui/fidget.nvim" },
    },
  },

  -- LSP Adapters
  {
    "ray-x/go.nvim",
    event = "CmdlineEnter",
    ft = { "go", "gomod" },
    build = function()
      require("go.install").update_all_sync()
    end,
    config = function()
      require "plugins.go"
    end,
  },

  -- Snippet
  {
    "L3MON4D3/LuaSnip",
    event = "InsertEnter",
    dependencies = {
      { "rafamadriz/friendly-snippets" },
    },
  },

  -- Completion
  {
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    dependencies = {
      { "hrsh7th/cmp-nvim-lua" },
      { "hrsh7th/cmp-nvim-lsp" },
      { "hrsh7th/cmp-buffer" },
      { "hrsh7th/cmp-path" },
      { "hrsh7th/cmp-cmdline" },
      { "hrsh7th/cmp-calc" },
      { "saadparwaiz1/cmp_luasnip" },
      { "petertriho/cmp-git" },
    },
    config = function()
      require "plugins.cmp"
    end,
  },

  -- Addons for LSP
  {
    "ThePrimeagen/refactoring.nvim",
    event = { "BufRead", "BufNewFile" },
  },
  {
    "folke/trouble.nvim",
    event = { "BufRead", "BufNewFile" },
    config = function()
      require "plugins.trouble"
    end,
  },
  {
    "stevearc/dressing.nvim",
    event = "VeryLazy",
    config = function()
      require "plugins.dressing"
    end,
  },

  -- Linter
  {
    "mfussenegger/nvim-lint",
    event = { "BufRead", "BufNewFile" },
  },

  -- Formatter
  {
    "mhartington/formatter.nvim",
    event = { "BufRead", "BufNewFile" },
  },

  -- Debugger
  {
    "mfussenegger/nvim-dap",
    lazy = false,
    dependencies = {
      { "rcarriga/nvim-dap-ui" },
      { "theHamsta/nvim-dap-virtual-text" },
    },
  },

  -- Debugger Adapters
  { "leoluz/nvim-dap-go" },

  -- Terminal
  {
    "akinsho/toggleterm.nvim",
    version = "*",
    event = "VimEnter",
    config = function()
      require("toggleterm").setup {
        direction = "float",
        open_mapping = [[<c-\>]],
      }
    end,
  },

  -- Markdown Preview
  {
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = { "markdown" },
    build = function()
      vim.fn["mkdp#util#install"]()
    end,
  },

  -- Git
  {
    "ThePrimeagen/git-worktree.nvim",
    lazy = false,
  },
  {
    "tpope/vim-fugitive",
    lazy = false,
  },
  {
    "lewis6991/gitsigns.nvim",
    lazy = false,
    config = function()
      require("plugins.gitsigns")
    end,
  },

  -- Rest client
  {
    "rest-nvim/rest.nvim",
    ft = "http",
    config = function()
      require("rest-nvim").setup({
        formatters = {
          json = "jq",
          html = function(body)
            return vim.fn.system({"tidy", "-i", "-q", "-"}, body)
          end
        },
      })
    end,
  },

  -- TODO: Добавить поддержку баз данных
  -- TODO: Добавить поддержку заметок
}
