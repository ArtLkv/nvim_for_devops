local hl = vim.api.nvim_set_hl

hl(0, "DevOopsHeaderLogo", { bold = true, fg = "#A2B927" })
hl(0, "DevOopsHeaderDatetime", { bold = true, fg = "#A2B927" })
hl(0, "DevOopsFooter", { bold = true, fg = "#A2B927" })
