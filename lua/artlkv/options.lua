local opts = {
  autoindent = true,
  autoread = true,
  backspace = "indent,eol,start",
  backup = false,
  clipboard = "unnamed,unnamedplus",
  cmdheight = 0,
  completeopt = "menu,menuone,noselect",
  conceallevel = 2,
  concealcursor = "",
  emoji = false,
  expandtab = true,
  errorbells = false,
  encoding = "utf-8",
  foldcolumn = "0",
  foldnestmax = 0,
  foldlevel = 99,
  foldlevelstart = 99,
  fileencoding = "utf-8",
  hlsearch = true,
  ignorecase = true,
  incsearch = true,
  laststatus = 3,
  mouse = "a",
  number = true,
  pumheight = 10,
  relativenumber = true,
  scrolloff = 8,
  sidescrolloff = 8,
  softtabstop = 2,
  shiftwidth = 2,
  signcolumn = "yes",
  smartcase = true,
  smartindent = true,
  smarttab = true,
  splitright = true,
  swapfile = false,
  showmode = false,
  tabstop = 2,
  timeoutlen = 200,
  termguicolors = true,
  undofile = true,
  updatetime = 100,
  viminfo = "'1000",
  wildignore = "",
  wrap = false,
  writebackup = false,
}

local globals = {
  mapleader = " ",
  maplocalleader = " ",
}

for k, v in pairs(opts) do
  vim.opt[k] = v
end

for k, v in pairs(globals) do
  vim.g[k] = v
end
