local mason_ok, mason = pcall(require, "mason")
local mason_lsp_ok, mason_lsp = pcall(require, "mason-lspconfig")

if not mason_ok or not mason_lsp_ok then
  return
end

local icons = require "helpers.icons"

mason.setup {
  ui = {
    border = "rounded",
    icons = {
      package_installed = icons.packageUp,
      package_pending = icons.package,
      package_uninstalled = icons.packageDown,
    },
  },
  log_level = vim.log.levels.INFO,
  max_concurrent_installers = 4,
}

mason_lsp.setup {
  ensure_installed = {
    -- servers
    "lua_ls", -- Lua(extra)
    "bashls", -- Bash(main)
    "powershell_es", -- PowerShell(extra)
    "pyright", -- Python(main)
    "gopls", -- Golang(main)
    "clangd", -- C++(extra)
  },
  automatic_installation = false,
}

require("fidget").setup {}

require("lint").linters_by_ft = {
  lua = { "luacheck" },
  markdown = { "markdownlint" },
  sh = { "shellcheck" },
  python = { "pylint" },
  go = { "staticcheck" },
  cpp = { "cpplint" },
}

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
  callback = function()
    require("lint").try_lint()
  end,
})

-- formatters
require("formatter").setup {
  logging = true,
  log_level = vim.log.levels.WARN,
  filetype = {
    lua = {
      require("formatter.filetypes.lua").stylua,
    },
    sh = {
      require("formatter.filetypes.sh").shfmt,
    },
    go = {
      require("formatter.filetypes.go").gofmt,
      require("formatter.filetypes.go").goimports,
      require("formatter.filetypes.go").gofumpt,
      require("formatter.filetypes.go").golines,
    },
    python = {
      require("formatter.filetypes.python").autopep8,
    },
    cpp = {
      require("formatter.filetypes.cpp").clangformat,
    },
    ["*"] = {
      require("formatter.filetypes.any").remove_trailing_whitespace,
    },
  },
}

local lspconfig = require "lspconfig"

local handlers = {
  ["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
    silent = true,
    border = "rounded",
  }),
  ["textDocument/signatureHelp"] = vim.lsp.with(
    vim.lsp.handlers.signature_help,
    {
      border = "rounded",
    }
  ),
  ["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics,
    {
      virtual_text = true,
    }
  ),
}

local function on_attach(_, bufnr)
  local map = vim.keymap.set
  local opts = { buffer = bufnr }
  local describe = require("helpers.description").make_description

  -- Основные функции
  map("n", "<leader>rn", vim.lsp.buf.rename, describe(opts, "[R]e[n]"))
  map(
    "n",
    "<leader>ca",
    vim.lsp.buf.code_action,
    describe(opts, "[C]ode [A]ction")
  )
  map("n", "gd", vim.lsp.buf.definition, describe(opts, "[G]oto [D]efinition"))
  map(
    "n",
    "gr",
    require("telescope.builtin").lsp_references,
    describe(opts, "[G]oto [R]eferences")
  )
  map(
    "n",
    "gI",
    vim.lsp.buf.implementation,
    describe(opts, "[G]oto [I]mplementation")
  )
  map(
    "n",
    "<leader>D",
    vim.lsp.buf.type_definition,
    describe(opts, "Type [D]efinition")
  )
  map(
    "n",
    "<leader>ds",
    require("telescope.builtin").lsp_document_symbols,
    describe(opts, "[D]ocument [S]ymbols")
  )
  map(
    "n",
    "<leader>ws",
    require("telescope.builtin").lsp_dynamic_workspace_symbols,
    describe(opts, "[W]orkspace [S]ymbols")
  )

  -- Документация
  map("n", "K", vim.lsp.buf.hover, describe(opts, "Hover Documentation"))
  map(
    "n",
    "<C-k>",
    vim.lsp.buf.signature_help,
    describe(opts, "Signature Documentation")
  )

  -- Форматирование
  map("n", "<C-f>", "<cmd>Format<cr>", describe(opts, "[F]ormat"))

  -- Второстепенные(малоиспользуемые) функции
  map(
    "n",
    "gD",
    vim.lsp.buf.declaration,
    describe(opts, "[G]oto [D]eclaration")
  )
  map(
    "n",
    "<leader>wa",
    vim.lsp.buf.add_workspace_folder,
    describe(opts, "[W]orkspace [A]dd Folder")
  )
  map(
    "n",
    "<leader>wr",
    vim.lsp.buf.remove_workspace_folder,
    describe(opts, "[W]orkspace [R]emove Folder")
  )
  map("n", "<leader>wl", function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, describe(opts, "[W]orkspace [L]ist Folders"))
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)

capabilities.textDocument.foldingRange = {
  dynamicRegistration = false,
  lineFoldingOnly = true,
}

lspconfig.lua_ls.setup {
  capabilities = capabilities,
  handlers = handlers,
  on_attach = on_attach,
  settings = require("servers.lua").settings,
}

lspconfig.powershell_es.setup {
  capabilities = capabilities,
  handlers = handlers,
  on_attach = on_attach,
  settings = require("servers.powershell").settings,
}

-- Если для сервера достаточно настроек по умолчанию
for _, server in ipairs { "bashls", "gopls", "pyright", "clangd" } do
  lspconfig[server].setup {
    on_attach = on_attach,
    capabilities = capabilities,
    handlers = handlers,
  }
end
