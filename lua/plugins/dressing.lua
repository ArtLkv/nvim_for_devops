require("dressing").setup {
  input = {
    enabled = true,
    default_prompt = "Input:",
    prompt_align = "left",
    insert_only = true,
    start_in_insert = true,
    border = "rounded",
    relative = "cursor",
    prefer_width = 10,
    width = nil,
    max_width = { 140, 0.9 },
    min_width = { 10, 0.1 },
    win_options = {
      winblend = 0,
      winhighlight = "",
    },
    mappings = {
      n = {
        ["<Esc>"] = "Close",
        ["<CR>"] = "Confirm",
      },
      i = {
        ["<C-c>"] = "Close",
        ["<CR>"] = "Confirm",
        ["<Up>"] = "HistoryPrev",
        ["<Down>"] = "HistoryNext",
      },
    },

    override = function(conf)
      return conf
    end,

    get_config = nil,
  },
  select = {
    enabled = true,
    backend = { "telescope", "nui", "fzf", "builtin" },
    nui = {
      position = {
        row = 1,
        col = 0,
      },
      size = nil,
      relative = "cursor",
      border = {
        style = "rounded",
        text = {
          top_align = "right",
        },
      },
      buf_options = {
        swapfile = false,
        filetype = "DressingSelect",
      },
      max_width = 80,
      max_height = 40,
    },

    builtin = {
      wnchor = "SW",
      border = "rounded",
      relative = "cursor",

      win_options = {
        winblend = 5,
        winhighlight = "",
      },

      width = nil,
      max_width = { 140, 0.8 },
      min_width = { 10, 0.2 },
      height = nil,
      max_height = 0.9,
      min_height = { 2, 0.05 },

      mappings = {
        ["<Esc>"] = "Close",
        ["<C-c>"] = "Close",
        ["<CR>"] = "Confirm",
      },

      override = function(conf)
        return conf
      end,
    },

    get_config = function(opts)
      if opts.kind == "codeaction" then
        return {
          backend = "builtin",
          nui = {
            relative = "cursor",
            max_width = 80,
            min_height = 2,
          },
        }
      end
    end,
  },
}
