local ok, starter = pcall(require, "alpha")
if not ok then
  return
end

local dashboard = require "alpha.themes.dashboard"
local icons = require "helpers.icons"

---------------------------------------------------------------
-- Header Logo
local logo = {
  "██████╗ ███████╗██╗   ██╗ ██████╗  ██████╗ ██████╗ ███████╗",
  "██╔══██╗██╔════╝██║   ██║██╔═══██╗██╔═══██╗██╔══██╗██╔════╝",
  "██║  ██║█████╗  ██║   ██║██║   ██║██║   ██║██████╔╝███████╗",
  "██║  ██║██╔══╝  ╚██╗ ██╔╝██║   ██║██║   ██║██╔═══╝ ╚════██║",
  "██████╔╝███████╗ ╚████╔╝ ╚██████╔╝╚██████╔╝██║     ███████║",
  "╚═════╝ ╚══════╝  ╚═══╝   ╚═════╝  ╚═════╝ ╚═╝     ╚══════╝",
}

dashboard.section.header.type = "text"
dashboard.section.header.val = logo
dashboard.section.header.opts = {
  position = "center",
  hl = "DevOopsHeaderLogo",
}

---------------------------------------------------------------
-- Header Datetime
local thingy =
  io.popen 'echo "$(LANG=en_us_88591; date +%a) $(date +%d) $(LANG=en_us_88591; date +%b)" | tr -d "\n"'
if thingy == nil then
  return
end

local date = "  " .. thingy:read "*a"
thingy:close()
local datetime = os.date " %H:%M"

local datetime_section = {
  type = "text",
  val = "────────────────── "
    .. date
    .. " | "
    .. datetime
    .. " ─────────────────",
  opts = {
    position = "center",
    hl = "DevOopsHeaderDatetime",
  },
}
---------------------------------------------------------------
-- Buttons
local leader = "SPC"

-- TODO: Add button for session manager
dashboard.section.buttons.val = {
  dashboard.button(
    leader .. " f f",
    icons.fileNoBg .. " [F]ind [F]iles",
    "<cmd>Telescope find_files<cr>"
  ),
  dashboard.button(
    leader .. " f o",
    icons.fileRecent .. " [F]ind [O]ld files",
    "<cmd>Telescope oldfiles<cr>"
  ),
  dashboard.button(
    leader .. " f g",
    icons.search .. " [F]ind with [G]rep",
    "<cmd>Telescope live_grep<cr>"
  ),
  dashboard.button(
    leader .. " f k",
    icons.word .. " [F]ind the [K]eymaps",
    "<cmd>Telescope keymaps<cr>"
  ),
  dashboard.button(
    leader .. " l",
    icons.packageDown .. " Open the [L]azy",
    "<cmd>Lazy<cr>"
  ),
  dashboard.button(
    leader .. " m",
    icons.consoleDebug .. " Open the [M]ason",
    "<cmd>Mason<cr>"
  ),
  dashboard.button(
    leader .. " q",
    icons.exit .. " [Q]uit without saving",
    "<cmd>qa!<cr>"
  ),
}

---------------------------------------------------------------
-- Footer
local function footer()
  local plugins = require("lazy").stats().count
  local v = vim.version()
  local author = "arthurlokhov@bk.ru"
  return string.format(
    " v%d.%d.%d  󰂖 %d    %s ",
    v.major,
    v.minor,
    v.patch,
    plugins,
    author
  )
end

dashboard.section.footer.val = {
  footer(),
}
dashboard.section.footer.opts = {
  position = "center",
  hl = "DevOopsFooter",
}

---------------------------------------------------------------
local section = {
  header = dashboard.section.header,
  datetime_section = datetime_section,
  buttons = dashboard.section.buttons,
  footer = dashboard.section.footer,
}

local opts = {
  layout = {
    { type = "padding", val = 3 },
    section.header,
    { type = "padding", val = 1 },
    section.datetime_section,
    { type = "padding", val = 2 },
    section.buttons,
    section.footer,
    { type = "padding", val = 3 },
  },
  opts = {
    margin = 5,
  },
}

-- TODO: Доделать приветственный экран: время, кнопки, сессии

starter.setup(opts)
