require("gitsigns").setup {
  signs = {
    add = { text = "+" },
    change = { text = "~" },
    delete = { text = "_" },
    topdelete = { text = "‾" },
    changedelete = { text = "~" },
  },
  current_line_blame = false,
  on_attach = function(bufnr)
    local gs = package.loaded.gitsigns
    local map = vim.keymap.set

    map("n", "]c", function()
      if vim.wo.diff then
        return "]c"
      end
      vim.schedule(function()
        gs.next_hunk()
      end)
      return "<Ignore>"
    end, { buffer = bufnr, expr = true })

    map("n", "[c", function()
      if vim.wo.diff then
        return "[c"
      end
      vim.schedule(function()
        gs.prev_hunk()
      end)
      return "<Ignore>"
    end, { buffer = bufnr, expr = true })

    -- Actions
    local describe = require("helpers.description").make_description
    map({ "n", "v" }, "<leader>hs", ":Gitsigns stage_hunk<CR>", describe({}, "[H]unk [S]tage"))
    map({ "n", "v" }, "<leader>hr", ":Gitsigns reset_hunk<CR>", describe({}, "[H]unk [R]eset"))
    map("n", "<leader>hS", gs.stage_buffer, describe({}, "[H]unk [S]tage Buffer"))
    map("n", "<leader>ha", gs.stage_hunk, describe({}, "[H]unk [A]ction"))
    map("n", "<leader>hu", gs.undo_stage_hunk, describe({}, "[H]unk [U]ndo Stage"))
    map("n", "<leader>hR", gs.reset_buffer, describe({}, "[H]unk [R]eset Buffer"))
    map("n", "<leader>hp", gs.preview_hunk, describe({}, "[H]unk [P]review"))
    map("n", "<leader>hb", function()
      gs.blame_line { full = true }
    end, describe({}, "[H]unk [B]lame"))
    map("n", "<leader>tb", gs.toggle_current_line_blame, describe({}, "[T]oggle Current line [B]lame"))
    map("n", "<leader>hd", gs.diffthis, describe({}, "[H]unk [D]iff"))
    map("n", "<leader>hD", function()
      gs.diffthis "~"
    end, describe({}, "[H]unk [D]iff"))
    map("n", "<leader>td", gs.toggle_deleted, describe({}, "[T]oggle [D]eleted"))

    map({ "o", "x" }, "ih", "<cmd>Gitsigns select_hunk<cr>", describe({}, "[I]nsert [H]unk"))
  end,
}
