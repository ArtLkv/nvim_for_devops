require("go").setup {
  max_line_len = 128,
  verbose = false,
  lsp_codelens = true,
  lsp_inlay_hints = {
    enable = true,
    only_current_line = false,
    show_variable_name = true,
    parameter_hints_prefix = "󰊕 ",
    show_parameter_hints = true,
    other_hints_prefix = "=> ",
    right_align = false,
  },
  trouble = true,
  luasnip = true,
}
