local ok, telescope = pcall(require, "telescope")
if not ok then
  return
end

telescope.load_extension "fzf"
telescope.load_extension "repo"
telescope.load_extension "file_browser"
telescope.load_extension "git_worktree"

local git_icons = {
  added = require("helpers.icons").gitAdd,
  changed = require("helpers.icons").gitChange,
  copied = ">",
  deleted = require("helpers.icons").gitRemove,
  renamed = "➡",
  unmerged = "‡",
  untracked = "?",
}

telescope.setup {
  pickers = {
    find_files = {
      hidden = true,
    },
  },
  defaults = {
    file_ignore_patterns = {
      "^./.git/",
      "^node_modules/",
    },
    border = true,
    hl_result_eol = true,
    multi_icon = "",
    vimgrep_arguments = {
      "rg",
      "--color=never",
      "--no-heading",
      "--with-filename",
      "--line-number",
      "--column",
      "--smart-case",
    },
    file_sorter = require("telescope.sorters").get_fzy_sorter,
    prompt_prefix = "  ",
    color_devicons = true,
    git_icons = git_icons,
    sorting_strategy = "ascending",
    mappings = {
      i = {
        ["<C-u>"] = false,
        ["<C-d>"] = false,
        ["<C-j>"] = require("telescope.actions").move_selection_next,
        ["<C-k>"] = require("telescope.actions").move_selection_previous,
      },
    },
  },
}
