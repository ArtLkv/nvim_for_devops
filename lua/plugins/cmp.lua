local icons = require "helpers.icons"
local helpers = require "helpers.lsp"

local cmp_status_ok, cmp = pcall(require, "cmp")
if not cmp_status_ok then
  return
end

local snip_status_ok, luasnip = pcall(require, "luasnip")
if not snip_status_ok then
  return
end

local cmp_git_ok, cmp_git = pcall(require, "cmp_git")
if not cmp_git_ok then
  return
end

cmp_git.setup {}
require("luasnip/loaders/from_vscode").lazy_load()

local source_mapping = {
  nvim_lsp = icons.stack .. "LSP",
  nvim_lua = icons.bomb,
  buffer = icons.buffer .. "BUF",
  luasnip = icons.snippet .. "SNP",
  calc = icons.calculator,
  path = icons.folderOpen2,
  treesitter = icons.tree,
  zsh = icons.terminal .. "ZSH",
}

cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert {
    ["<C-k>"] = cmp.mapping.select_prev_item(),
    ["<C-j>"] = cmp.mapping.select_next_item(),
    ["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(-2), { "i", "c" }),
    ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(2), { "i", "c" }),
    ["<C-Space>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),
    ["<C-y>"] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
    ["<C-e>"] = cmp.mapping {
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    },
    ["<CR>"] = cmp.mapping.confirm {
      -- this is the important line for Copilot
      behavior = cmp.ConfirmBehavior.Replace,
      -- select = EcoVim.plugins.completion.select_first_on_enter,
    },
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif cmp.visible() and helpers.has_words_before() then
        cmp.select_next_item { behavior = cmp.SelectBehavior.Select }
      elseif luasnip.expandable() then
        luasnip.expand()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif helpers.check_backspace() then
        fallback()
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
    ["<C-l>"] = cmp.mapping(function(fallback)
      if luasnip.expandable() then
        luasnip.expand()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
    ["<C-h>"] = cmp.mapping(function(fallback)
      if luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
  },
  formatting = {
    format = function(entry, vim_item)
      local item_with_kind = require("lspkind").cmp_format {
        mode = "symbol_text",
        maxwidth = 50,
        symbol_map = source_mapping,
      }(entry, vim_item)

      item_with_kind.menu = source_mapping[entry.source.name]
      item_with_kind.menu = vim.trim(item_with_kind.menu or "")

      return item_with_kind
    end,
  },
  sources = {
    {
      name = "nvim_lsp",
      priority = 10,
      entry_filter = helpers.limit_lsp_types,
    },
    {
      name = "git",
      priority = 7,
    },
    {
      name = "luasnip",
      priority = 7,
      max_item_count = 5,
    },
    {
      name = "buffer",
      priority = 7,
      keyword_length = 5,
      max_item_count = 10,
      option = helpers.buffer_option,
    },
    { name = "nvim_lua", priority = 5 },
    { name = "path", priority = 4 },
    { name = "calc", priority = 3 },
  },
  sorting = {
    priority_weight = 2,
    comparators = {
      helpers.deprioritize_snippet,
      cmp.config.compare.exact,
      cmp.config.compare.locality,
      cmp.config.compare.score,
      cmp.config.compare.recently_used,
      cmp.config.compare.offset,
      cmp.config.compare.sort_text,
      cmp.config.compare.order,
    },
  },
  confirm_opts = {
    behavior = cmp.ConfirmBehavior.Replace,
    select = false,
  },
  window = {
    completion = cmp.config.window.bordered {
      winhighlight = "NormalFloat:NormalFloat,FloatBorder:FloatBorder",
    },
    documentation = cmp.config.window.bordered {
      winhighlight = "NormalFloat:NormalFloat,FloatBorder:FloatBorder",
    },
  },
  experimental = {
    ghost_text = true,
  },
  performance = {
    max_view_entries = 100,
  },
}
