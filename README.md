# Конфигурация NeoVim для DevOps разработки

## Поддерживаемые языки программирования

1. Golang и Python являются основными языками программирования в сфере DevOps.
2. Bash и PowerShell необходимы для автоматизации.
3. Nix - требуется для поддержки NixOs/NixPkg конфигураций.
4. C++ - нужны для некоторых инструментов DevOps.

- [X] Golang
  - [X] LSP - `gopls`
  - [X] Dap - `delve`
  - [X] Linter - `staticcheck`
  - [X] Formatter - `gofmt`, `goimports`, `gofumpt`, `golines`
- [ ] Python
  - [X] LSP - `pyright`
  - [ ] Dap - `???`
  - [X] Linter - `pylint`
  - [X] Formatter - `autopep8`
- [ ] Nix
  - [ ] LSP - `nil`
  - [X] Dap: `Don't supported`
  - [X] Linter: `Don't supported`
  - [X] Formatter: `Don't supported`
- [ ] C++
  - [X] LSP - `clangd`
  - [ ] Dap - `???`
  - [X] Linter - `cpplint`
  - [X] Formatter - `clangformat`
- [ ] Bash
  - [X] LSP - `bash-language-server`
  - [ ] Dap - `???`
  - [X] Linter - `shellcheck`
  - [X] Formatter - `shfmt`
- [X] PowerShell
  - [X] LSP - `powershell-editor-services`
  - [X] Dap: `Don't supported`
  - [X] Linter: `Don't supported`
  - [X] Formatter: `Don't supported`

## Текущий функционал

- [X] Language Server Protocol(nvim-lspconfig, mason.nvim, go.nvim)
  - [X] Syntax Highlighting(nvim-treesitter)
  - [X] Auto-completion(nvim-cmp and him plugins)
  - [X] Snippets(LuaSnip, friendly-snippets)
  - [X] Refactoring(refactoring.nvim)
  - [X] Linting(nvim-lint)
  - [X] Formatting(formatter.nvim)
  - [X] Diagnostics(trouble.nvim)
- [X] Debugger(nvim-dap, nvim-dap-ui)
- [X] Navigation(telescope.nvim)
  - [X] File Browser(telescope-file-browser.nvim)
- [X] Beautiful UI(dressing.nvim, nvim-notify, noice.nvim)
  - [X] Transparent UI support(transparent-toggle)
- [X] Statusline(lualine.nvim)
- [X] Comments support(Comment.nvim, todo-comments.nvim)
- [X] Integrated terminal
- [X] Git integration
- [X] Markdown Preview
- [ ] Remote Development
- [ ] AI assistant ??
- [ ] Database manager and sql support
- [ ] Org/Neorg tips support
- [X] Rest client(rest.nvim)
- [ ] Configs support
  - [ ] Yaml
  - [X] Toml(Only highlight, LSP not yet stable)
  - [ ] Json
  - [ ] Dotenv
  - [ ] Xml
  - [ ] Groovy
- [ ] DevOps Tools
  - [ ] Docker
  - [ ] Kubernetes
  - [ ] Terraform
  - [ ] Ansible
  - [ ] Services Intergration
  - [ ] Github Actions
  - [ ] Gitlab
  - [ ] Jenkins ??
- [ ] Installation script for DevOops(powershell for windows, bash for unix)

## Скриншоты

![Greeter](./assets/nv1.png)
![Mason](./assets/nv2.png)
![Package Manager](./assets/nv3.png)
![Navigation](./assets/nv4.png)
![Keymaps](./assets/nv5.png)
![Linter](./assets/nv6.png)
